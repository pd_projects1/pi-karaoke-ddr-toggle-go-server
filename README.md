# pi-firesoundbar-endpoint



## What is this

Golang toggle daemon for my Raspberry Pi, launches and stops pikaraoke, launches and stops OutFox.

## Endpoints
/karaokeon - start pikaraoke and sound loopback  
/karaokeoff - stop pikaraoke and sound loopback  
/danceon - start Project OutFox  
/danceoff - stop Project OutFox

## Considerations
Tested on Rpi4 w Raspberry Pi OS.  You'll have to edit or the pactl commands to customize for your mic.

## License
This project is licensed under the AGPLv3 License - see the LICENSE file for details.
