package main

import (
    "fmt"
    "net/http"
    "os/exec"
    "strconv"
    "strings"
    "sync"
    "syscall"
)

var (
    karaokeCmd *exec.Cmd
    danceCmd   *exec.Cmd
    mutex      sync.Mutex
    loopbackModuleIndex int
)

func main() {
    http.HandleFunc("/karaokeon", karaokeOnHandler)
    http.HandleFunc("/karaokeoff", karaokeOffHandler)
    http.HandleFunc("/danceon", danceOnHandler)
    http.HandleFunc("/danceoff", danceOffHandler)

    fmt.Println("Server started on :8080")
    http.ListenAndServe(":8080", nil)
}

func karaokeOnHandler(w http.ResponseWriter, r *http.Request) {
    mutex.Lock()
    defer mutex.Unlock()

    if karaokeCmd != nil && karaokeCmd.Process != nil {
        fmt.Fprintf(w, "Karaoke is already running")
        return
    }

    // Start the karaoke command
    karaokeCmd = exec.Command("/home/pi/pikaraoke/pikaraoke.sh")
    karaokeCmd.Env = append(karaokeCmd.Env, "DISPLAY=:0")
    karaokeCmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
    err := karaokeCmd.Start()
    if err != nil {
        fmt.Fprintf(w, "Error starting karaoke: %v", err)
        return
    }

    // Start the audio loopback
    loopbackCmd := exec.Command("/usr/bin/pactl", "load-module", "module-loopback", "source=alsa_input.usb-Logitech_USB_Microphone_20170726905926-00.mono-fallback", "sink=alsa_output.platform-fef00700.hdmi.hdmi-stereo")
    output, err := loopbackCmd.CombinedOutput()
    if err != nil {
        fmt.Fprintf(w, "Error starting loopback: %v", err)
        return
    }

    // Parse and store the loopback module index
    outputStr := strings.TrimSpace(string(output))
    loopbackModuleIndex, err = strconv.Atoi(outputStr)
    if err != nil {
        fmt.Fprintf(w, "Error parsing loopback module index: %v", err)
        return
    }

    fmt.Fprintf(w, "Karaoke and audio loopback started")
}

func karaokeOffHandler(w http.ResponseWriter, r *http.Request) {
    mutex.Lock()
    defer mutex.Unlock()

    if karaokeCmd == nil || karaokeCmd.Process == nil {
        fmt.Fprintf(w, "Karaoke is not running")
        return
    }

    // Stop the karaoke command
    err := syscall.Kill(-karaokeCmd.Process.Pid, syscall.SIGKILL)
    if err != nil {
        fmt.Fprintf(w, "Error stopping karaoke: %v", err)
    }

    // Stop the audio loopback
    if loopbackModuleIndex > 0 {
        loopbackCmd := exec.Command("/usr/bin/pactl", "unload-module", strconv.Itoa(loopbackModuleIndex))
        _, err := loopbackCmd.CombinedOutput()
        if err != nil {
            fmt.Fprintf(w, "Error stopping loopback: %v", err)
        }
    }

    karaokeCmd = nil
    loopbackModuleIndex = 0
    fmt.Fprintf(w, "Karaoke and audio loopback stopped")
}

func danceOnHandler(w http.ResponseWriter, r *http.Request) {
    mutex.Lock()
    defer mutex.Unlock()

    if danceCmd != nil && danceCmd.Process != nil {
        fmt.Fprintf(w, "Dance is already running")
        return
    }

    danceCmd = exec.Command("/home/pi/OutFox/OutFox")
    danceCmd.Env = append(danceCmd.Env, "DISPLAY=:0")
    err := danceCmd.Start()
    if err != nil {
        fmt.Fprintf(w, "Error starting dance: %v", err)
    } else {
        fmt.Fprintf(w, "Dance started")
    }
}

func danceOffHandler(w http.ResponseWriter, r *http.Request) {
    mutex.Lock()
    defer mutex.Unlock()

    if danceCmd == nil || danceCmd.Process == nil {
        fmt.Fprintf(w, "Dance is not running")
        return
    }

    err := danceCmd.Process.Kill()
    if err != nil {
        fmt.Fprintf(w, "Error stopping dance: %v", err)
    } else {
        danceCmd = nil
        fmt.Fprintf(w, "Dance stopped")
    }
}
